const { app, BrowserWindow, ipcMain } = require('electron')
const path = require('path')
const ipc = ipcMain

function createWindow () {
  const win = new BrowserWindow({
    width: 300,
    height: 550,
    // resizable: false,
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      devTools: true,
      preload: path.join(__dirname, 'preload.js')
    }
  })

  win.loadFile('src/index.html')

ipc.on('closeWindow', ()=>{   //close window event
  win.close()
})

// ipc.on('maximizeWindow', ()=>{    //maximize window event
//   if(win.isMaximized()){
//     win.unmaximize()
//   }
//   else{
//     win.maximize()
//   }
// })
// win.on('maximize', ()=>{
//   win.webContents.send('maximizedWindow')
// })
// win.on('unmaximize', ()=>{
//   win.webContents.send('unMaxWindow')
// })

ipc.on('minimizeWindow', ()=>{    //minimize window event
    win.minimize()
  })
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})