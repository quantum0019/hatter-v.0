const { ipcRenderer } = require("electron")
const ipc = ipcRenderer
const MaxWindowBtn = document.getElementById('maximizeWindow')

closeWindow.addEventListener('click', ()=>{
    ipc.send('closeWindow')
})

minimizeWindow.addEventListener('click', ()=>{
    ipc.send('minimizeWindow')
})

// maximizeWindow.addEventListener('click', ()=>{
//     ipc.send('maximizeWindow')
// })
// function changeMaxWindowIcon(isMaximizedApp){
//     if(isMaximizedApp){
//         MaxWindowBtn.classList.remove('maximize-btn')
//         MaxWindowBtn.classList.add('top-btn')
//     }
//     else{
//         MaxWindowBtn.classList.remove('top-btn')
//         MaxWindowBtn.classList.add('maximize-btn')
//     }
// }
// ipc.on('maximizedWindow', ()=>{changeMaxWindowIcon(true)})
// ipc.on('unMaxWindow', ()=>{changeMaxWindowIcon(false)})